package fr.cnam.foad.nfa035.exam.model;


import java.util.Date;

public class MyWhiteExam {

    private String intitule;
    private String codeUe;
    private String ecole;
    private String region;
    private Date date;

    public MyWhiteExam() {
    }


    public MyWhiteExam(String intitule, String codeUe, String ecole, String region, Date date) {
        this.intitule = intitule;
        this.codeUe = codeUe;
        this.ecole = ecole;
        this.region = region;
        this.date = date;
    }


    public String getIntitule() {
        return intitule;
    }


    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }


    public String getCodeUe() {
        return codeUe;
    }


    public void setCodeUe(String codeUe) {
        this.codeUe = codeUe;
    }


    public String getEcole() {
        return ecole;
    }


    public void setEcole(String ecole) {
        this.ecole = ecole;
    }


    public String getRegion() {
        return region;
    }


    public void setRegion(String region) {
        this.region = region;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MyExam{" +
                "intitule='" + intitule + '\'' +
                ", codeUe='" + codeUe + '\'' +
                ", ecole='" + ecole + '\'' +
                ", region='" + region + '\'' +
                ", date=" + date.getTime() +
                '}';
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((intitule == null) ? 0 : intitule.hashCode());
        result = prime * result + ((codeUe == null) ? 0 : codeUe.hashCode());
        result = prime * result + ((ecole == null) ? 0 : ecole.hashCode());
        result = prime * result + ((region == null) ? 0 : region.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MyWhiteExam other = (MyWhiteExam) obj;
        if (intitule == null) {
            if (other.intitule != null)
                return false;
        } else if (!intitule.equals(other.intitule))
            return false;
        if (codeUe == null) {
            if (other.codeUe != null)
                return false;
        } else if (!codeUe.equals(other.codeUe))
            return false;
        if (ecole == null) {
            if (other.ecole != null)
                return false;
        } else if (!ecole.equals(other.ecole))
            return false;
        if (region == null) {
            if (other.region != null)
                return false;
        } else if (!region.equals(other.region))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        return true;
    }


    
}
