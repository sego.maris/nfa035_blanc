package fr.cnam.foad.nfa035.exam.pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.cnam.foad.nfa035.exam.model.MyWhiteExam;

@Component(value = "exam-encours")
public class MyExamDelegate {

    @Autowired
    private MyWhiteExam exam;

    public String delegateToString() {
        return this.exam.toString(); 
    }
}
